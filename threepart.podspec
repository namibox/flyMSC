Pod::Spec.new do |s|
  s.name         = "iflyMSC"
  s.version      = "1.0"
  s.homepage     = "https://git.oschina.net/namibox"
  s.author       = { "iflyMSC" => "xiangying" }
  s.summary      = "科大讯飞语音识别SDK、支付宝、银联applepaysdk"

  s.platform     =  :ios, "7.0"
  s.source       = { :git => "https://git.oschina.net/namibox/iflyMSC.git", :tag => "1.0" }

  s.subspec "ifly" do |ifly|
    ifly.subspec "ise" do |ise|
    ise.source_files  = "ifly/ise/*.{h,m}"
    end

    ifly.subspec "results" do |results|
    results.source_files  = "ifly/ise/results/*.{h,m}"
    end

    ifly.subspec "pcm" do |pcm|
    pcm.source_files  = "ifly/pcm/*.{h,m}"
    end

    ifly.vendored_frameworks = "ifly/iflyMSC.framework"
 end

  s.subspec "alipay" do |alipay|
    alipay.source_files  = "alipay/AlipaySDK.bundle"
    alipay.vendored_frameworks = "alipay/AlipaySDK.framework"
  end

  s.frameworks   =  "CoreLocation","CoreTelephony","AddressBook","AudioToolbox","AVFoundation","SystemConfiguration"
  s.requires_arc = true

end
